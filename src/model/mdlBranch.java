package model;

import com.google.gson.annotations.SerializedName;

public class mdlBranch {
    @SerializedName(value = "BranchCode", alternate = {"branch_code", "branch_cd"})
    public String BranchCode;
    @SerializedName(value = "BranchName", alternate = "branch_name")
    public String BranchName;
    @SerializedName(value = "BranchTypeID", alternate = {"branch_type", "branch_type_id"})
    public String BranchTypeID;
    @SerializedName(value = "BranchTypeName", alternate = "branch_type_name")
    public String BranchTypeName;
    @SerializedName(value = "BranchInitial", alternate = "branch_initial")
    public String BranchInitial;
    @SerializedName(value = "BranchCoordinator", alternate = "branch_coordinator")
    public String BranchCoordinator;
    @SerializedName(value = "LocationType", alternate = "location_type")
    public String LocationType;
    @SerializedName(value = "RegionCode", alternate = {"region_code", "region_cd"})
    public String RegionCode;
    @SerializedName(value = "Address", alternate = {"address", "branch_address"})
    public String Address;
    @SerializedName(value = "City", alternate = {"city", "branch_city"})
    public String City;
    @SerializedName(value = "Longitude", alternate = {"longitude", "branch_longitude"})
    public String Longitude;
    @SerializedName(value = "Latitude", alternate = {"latitude", "branch_latitude"})
    public String Latitude;
    @SerializedName(value = "FlagReservation", alternate = {"flag_reservation", "reservation"})
    public String FlagReservation;
    @SerializedName(value = "FlagWeekendBankingSaturday", alternate = {"flag_weekend_banking_saturday", "weekend_bank_sat"})
    public String FlagWeekendBankingSaturday;
    @SerializedName(value = "FlagWeekendBankingSunday", alternate = {"flag_weekend_banking_sunday", "weekend_bank_sun"})
    public String FlagWeekendBankingSunday;
    @SerializedName(value = "VendorKiosk", alternate = {"vendor_kiosk", "kiosk_vendor"})
    public String VendorKiosk;
    @SerializedName(value = "RegularKiosk", alternate = {"regular_kiosk", "reguler_kiosk"})
    public String RegularKiosk;
    @SerializedName(value = "PrioritasKiosk", alternate = "prioritas_kiosk")
    public String PrioritasKiosk;
    @SerializedName(value = "Timezone", alternate = "timezone")
    public String Timezone;
    @SerializedName(value = "KcuCode", alternate = "kcu_cd")
    public String KcuCode;
    @SerializedName(value = "KcuName", alternate = "kcu_name")
    public String KcuName;
    @SerializedName(value = "UpdatedDate", alternate = "updated_date")
    public String UpdatedDate;
}
