package model;

import com.google.gson.annotations.SerializedName;

public class mdlAPIResult {
    @SerializedName(value = "ErrorSchema", alternate = "error_schema")
    public model.mdlErrorSchema ErrorSchema;
    @SerializedName(value = "OutputSchema", alternate = "output_schema")
    public Object OutputSchema;
}