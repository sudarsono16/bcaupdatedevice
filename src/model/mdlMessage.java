package model;

import com.google.gson.annotations.SerializedName;

public class mdlMessage {
    	@SerializedName(value = "Indonesian", alternate = "indonesian")
	public String Indonesian;
    	@SerializedName(value = "English", alternate = "english")
	public String English;
}