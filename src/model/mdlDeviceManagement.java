package model;

public class mdlDeviceManagement {
    public String SerialNumber;
    public String WSID;
    public String BranchCode;
    public String BranchTypeID;
    public String BranchInitial;
    public String BranchName;
    public String BranchTypeName;
    public String IsMaster;
    public String IsMyBca;
    public String IpPrinter;
    public String UpdateDate;
    public String UpdateBy;
    public String Overide;
    public String DeleteBranch;
}
