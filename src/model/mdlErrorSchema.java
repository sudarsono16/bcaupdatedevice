package model;

import com.google.gson.annotations.SerializedName;

public class mdlErrorSchema {
    @SerializedName(value = "ErrorCode", alternate = "error_code")
    public String ErrorCode;
    @SerializedName(value = "ErrorMessage", alternate = "error_message")
    public model.mdlMessage ErrorMessage;
}
