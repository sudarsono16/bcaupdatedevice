package com.bca.controller;

import com.google.gson.reflect.TypeToken;
import model.mdlResultPing;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import adapter.DeviceManagementAdapter;
import adapter.ErrorAdapter;
import adapter.LogAdapter;
import model.mdlAPIResult;
import model.mdlBranch;

import java.lang.reflect.Type;
import java.util.List;

@RestController
public class controller {
    final static Logger logger = LogManager.getLogger(controller.class);
    static Gson gson = new Gson();

    @RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String GetPing() {

        mdlAPIResult mdlPingGetBranchResult = new mdlAPIResult();
        mdlResultPing mdlResultPing = new mdlResultPing();
        mdlResultPing.result = "OK";
        mdlPingGetBranchResult.ErrorSchema = ErrorAdapter.getErrorSchemaPing("00");
        mdlPingGetBranchResult.OutputSchema = mdlResultPing.result;
        return gson.toJson(mdlPingGetBranchResult);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody model.mdlAPIResult UpdateDeviceManagement(@RequestBody String jsonIn) {
        long startTime = System.currentTimeMillis();
        model.mdlAPIResult mdlUpdateDeviceManagementResult = new model.mdlAPIResult();
        model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
        model.mdlMessage mdlMessage = new model.mdlMessage();
        model.mdlResult mdlResult = new model.mdlResult();
        String apiUrl = "/BCAUpdateDevice/update";
        String apimethod = "POST";
        model.mdlDeviceManagement param = gson.fromJson(jsonIn, model.mdlDeviceManagement.class);

        model.mdlLog mdlLog = new model.mdlLog();
        mdlLog.WSID = param.WSID;
        mdlLog.SerialNumber = param.SerialNumber;
        mdlLog.ApiFunction = "updateDeviceManagement";
        mdlLog.SystemFunction = "UpdateDeviceManagement";
        mdlLog.LogSource = "Webservice";
        mdlLog.LogStatus = "Failed";
        mdlResult.Result = "false";
        boolean checkDeviceManagement, checkWSIDBranchCode = false;
        Boolean isSuccess = false;
        Integer checkWSID = 1;
        mdlBranch mdlBranch = new mdlBranch();

        try {
            model.mdlDeviceManagement previousDeviceData = DeviceManagementAdapter.GetDeviceData(param.SerialNumber, param.WSID);

            //check new device branch data
            mdlAPIResult getInquiryBranchResult = DeviceManagementAdapter.hitApiInquiryBranch(param.BranchCode, param.SerialNumber, param.WSID);
            if (getInquiryBranchResult == null || getInquiryBranchResult.ErrorSchema == null) {
                mdlUpdateDeviceManagementResult.ErrorSchema = ErrorAdapter.GetErrorSchema("06");
                mdlLog.ErrorMessage = mdlUpdateDeviceManagementResult.ErrorSchema.ErrorMessage.English;
                mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
                logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiUrl, apimethod, "", jsonIn, gson.toJson(mdlUpdateDeviceManagementResult)));
                LogAdapter.InsertLog(mdlLog);
                return mdlUpdateDeviceManagementResult;
            } else {
                if (!getInquiryBranchResult.ErrorSchema.ErrorCode.equals("ESB-00-000")) {
                    mdlUpdateDeviceManagementResult.ErrorSchema = getInquiryBranchResult.ErrorSchema;
                    mdlLog.ErrorMessage = mdlUpdateDeviceManagementResult.ErrorSchema.ErrorMessage.English;
                    logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiUrl, apimethod, "getInquiryBranchResult: " + gson.toJson(getInquiryBranchResult), jsonIn, gson.toJson(mdlUpdateDeviceManagementResult)));
                    LogAdapter.InsertLog(mdlLog);
                    return mdlUpdateDeviceManagementResult;
                } else {
                    String branchDataString = gson.toJson(getInquiryBranchResult.OutputSchema);
                    Type inquiryBranchType = new TypeToken<List<mdlBranch>>() {
                    }.getType();
                    List<mdlBranch> inquiryBranchList = gson.fromJson(branchDataString, inquiryBranchType);
                    if (inquiryBranchList == null || inquiryBranchList.size() == 0) {
                        // there is no branch data in API inquiry branch EAI
                        mdlUpdateDeviceManagementResult.ErrorSchema = ErrorAdapter.GetErrorSchema("01");
                        mdlLog.ErrorMessage = mdlUpdateDeviceManagementResult.ErrorSchema.ErrorMessage.English;
                        logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiUrl, apimethod, "getInquiryBranchResult: " + gson.toJson(getInquiryBranchResult), jsonIn, gson.toJson(mdlUpdateDeviceManagementResult)));
                        LogAdapter.InsertLog(mdlLog);
                        return mdlUpdateDeviceManagementResult;
                    } else {
                        String branchCode = param.BranchCode;
                        String branchType = param.BranchTypeID;
                        String branchInitial = param.BranchInitial;
                        // search new branch data from branch data list EAI returned
                        mdlBranch = inquiryBranchList.stream().filter(branch -> branch.BranchCode.substring(0, 4).equals(branchCode)
                                && branch.BranchTypeID.equalsIgnoreCase(branchType)
                                && branch.BranchInitial.equalsIgnoreCase(branchInitial)).findAny().orElse(null);
                        if (mdlBranch == null || mdlBranch.BranchCode == null || mdlBranch.BranchTypeID == null || mdlBranch.BranchInitial == null) {
                            // there is no branch data in EAI branch data
                            mdlUpdateDeviceManagementResult.ErrorSchema = ErrorAdapter.GetErrorSchema("08");
                            mdlLog.ErrorMessage = mdlUpdateDeviceManagementResult.ErrorSchema.ErrorMessage.English;
                            logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiUrl, apimethod, "getInquiryBranchResult: " + gson.toJson(getInquiryBranchResult), jsonIn, gson.toJson(mdlUpdateDeviceManagementResult)));
                            LogAdapter.InsertLog(mdlLog);
                            return mdlUpdateDeviceManagementResult;
                        } else {
                            // trim branch code to only 4 digits (in case branch code is more than 4 digit ex :0021KBK
                            mdlBranch.BranchCode = mdlBranch.BranchCode.substring(0, 4);
                            // delete previous device and insert new branch data from EAI branch data to ms_branch in database
                            if (param.DeleteBranch != null && param.DeleteBranch.equals("1")) {
                                DeviceManagementAdapter.DeleteBranchData(previousDeviceData, param.SerialNumber, param.WSID);
                            }
                            boolean successUpdateBranchData = DeviceManagementAdapter.UpdateBranchData(previousDeviceData, mdlBranch, param.SerialNumber, param.WSID);
                            if (!successUpdateBranchData) {
                                mdlUpdateDeviceManagementResult.ErrorSchema = ErrorAdapter.GetErrorSchema("07");
                                mdlLog.ErrorMessage = mdlUpdateDeviceManagementResult.ErrorSchema.ErrorMessage.English;
                                logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiUrl, apimethod, "getInquiryBranchResult: " + gson.toJson(getInquiryBranchResult), jsonIn, gson.toJson(mdlUpdateDeviceManagementResult)));
                                LogAdapter.InsertLog(mdlLog);
                                return mdlUpdateDeviceManagementResult;
                            }
                        }
                    }
                }
            }

            checkWSIDBranchCode = DeviceManagementAdapter.CheckWSIDBranchCode(param.BranchCode, param.WSID, param.SerialNumber);
            if (!checkWSIDBranchCode) {
                mdlUpdateDeviceManagementResult.ErrorSchema = ErrorAdapter.GetErrorSchema("04");
                mdlLog.ErrorMessage = mdlUpdateDeviceManagementResult.ErrorSchema.ErrorMessage.English;
                mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
                LogAdapter.InsertLog(mdlLog);
                return mdlUpdateDeviceManagementResult;
            }

            checkWSID = DeviceManagementAdapter.CheckWSID(param.SerialNumber, param.WSID);
            int overide = param.Overide == null || param.Overide.equals("") ? 0 : Integer.parseInt(param.Overide);
            if (overide == 0) {
                if (checkWSID == 1) {
                    mdlUpdateDeviceManagementResult.ErrorSchema = ErrorAdapter.GetErrorSchema("05");
                    mdlLog.ErrorMessage = mdlUpdateDeviceManagementResult.ErrorSchema.ErrorMessage.English;
                    mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
                    LogAdapter.InsertLog(mdlLog);
                    return mdlUpdateDeviceManagementResult;
                } else if (checkWSID == 2) {
                    mdlUpdateDeviceManagementResult.ErrorSchema = ErrorAdapter.GetErrorSchema("06");
                    mdlLog.ErrorMessage = mdlUpdateDeviceManagementResult.ErrorSchema.ErrorMessage.English;
                    mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
                    LogAdapter.InsertLog(mdlLog);
                    return mdlUpdateDeviceManagementResult;
                }
            } else {
                if (checkWSID == 2) {
                    mdlUpdateDeviceManagementResult.ErrorSchema = ErrorAdapter.GetErrorSchema("06");
                    mdlLog.ErrorMessage = mdlUpdateDeviceManagementResult.ErrorSchema.ErrorMessage.English;
                    mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
                    LogAdapter.InsertLog(mdlLog);
                    return mdlUpdateDeviceManagementResult;
                }
            }

            checkDeviceManagement = DeviceManagementAdapter.CheckDeviceManagement(param.SerialNumber, param.WSID);
            if (overide == 1 && checkWSID == 1) {
                DeviceManagementAdapter.DeleteDeviceManagement(param);
            }
            if (checkDeviceManagement) {
                isSuccess = DeviceManagementAdapter.UpdateDeviceManagement(param);
            } else {
                isSuccess = DeviceManagementAdapter.InsertDeviceManagement(param);
            }

            if (isSuccess) {
                //check previous device branch data, if not exists, then delete branch data in ms_branch
                mdlAPIResult getPreviousBranchResult = DeviceManagementAdapter.hitApiInquiryBranch(previousDeviceData.BranchCode, param.SerialNumber, param.WSID);
                if (getPreviousBranchResult.ErrorSchema != null && getPreviousBranchResult.ErrorSchema.ErrorCode.equals("ESB-00-000")) {
                    String branchDataString = gson.toJson(getPreviousBranchResult.OutputSchema);
                    Type inquiryBranchType = new TypeToken<List<mdlBranch>>() {
                    }.getType();
                    List<mdlBranch> inquiryPreviousBranchList = gson.fromJson(branchDataString, inquiryBranchType);
                    if (inquiryPreviousBranchList != null && inquiryPreviousBranchList.size() > 0) {
                        // search previous branch data from previous branch data list EAI returned
                        mdlBranch previousBranch = inquiryPreviousBranchList.stream().filter(branch -> branch.BranchCode.substring(0, 4).equals(previousDeviceData.BranchCode)
                                && branch.BranchTypeID.equalsIgnoreCase(previousDeviceData.BranchTypeID)
                                && branch.BranchInitial.equalsIgnoreCase(previousDeviceData.BranchInitial)).findAny().orElse(null);
                        if (previousBranch == null || previousBranch.BranchCode == null || previousBranch.BranchCode.equals("")) {
                            DeviceManagementAdapter.DeleteBranchData(previousDeviceData, param.SerialNumber, param.WSID);
                        }
                    }
                }

                if (previousDeviceData.WSID != null && !previousDeviceData.WSID.isEmpty()) {
                    DeviceManagementAdapter.InsertDeviceManagementHistory(previousDeviceData, param);
                    mdlLog.ErrorMessage = "Old Data = WSID :" + previousDeviceData.WSID + ", BranchCode : " + previousDeviceData.BranchCode
                            + ", BranchTypeID : " + previousDeviceData.BranchTypeID + ", BranchInitial : " + previousDeviceData.BranchInitial
                            + ", IpPrinter : " + previousDeviceData.IpPrinter + ". New Data = WSID :" + param.WSID + ", BranchCode : " + param.BranchCode + ""
                            + ", BranchTypeID : " + param.BranchTypeID + ", BranchInitial : " + param.BranchInitial + ", IpPrinter : " + param.IpPrinter;
                }
                mdlUpdateDeviceManagementResult.ErrorSchema = ErrorAdapter.GetErrorSchema("00");
                mdlLog.LogStatus = mdlUpdateDeviceManagementResult.ErrorSchema.ErrorMessage.English;
                mdlResult.Result = isSuccess.toString();
                mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
                logger.info(LogAdapter.logControllerToLog4j(true, startTime, 200, apiUrl, apimethod, "", jsonIn, gson.toJson(mdlUpdateDeviceManagementResult)));
            } else {
                mdlErrorSchema.ErrorCode = "02";
                mdlMessage.Indonesian = "Data Gagal Disimpan";
                mdlMessage.English = mdlLog.ErrorMessage = "Save Data Failed";
                mdlErrorSchema.ErrorMessage = mdlMessage;
                mdlUpdateDeviceManagementResult.ErrorSchema = mdlErrorSchema;
                mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
                logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiUrl, apimethod, "", jsonIn, gson.toJson(mdlUpdateDeviceManagementResult)));
            }
        } catch (Exception ex) {
            mdlUpdateDeviceManagementResult.ErrorSchema = ErrorAdapter.GetErrorSchema("03");
            mdlLog.ErrorMessage = mdlUpdateDeviceManagementResult.ErrorSchema.ErrorMessage.English;
            mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
            logger.error(LogAdapter.logControllerToLog4jException(startTime, 500, apiUrl, apimethod, "", jsonIn, gson.toJson(mdlUpdateDeviceManagementResult), ex.toString()), ex);
        }
        LogAdapter.InsertLog(mdlLog);
        return mdlUpdateDeviceManagementResult;
    }

}
