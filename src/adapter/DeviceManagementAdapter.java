package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.bca.libapitoken.repository.ApiTokenRepository;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.ClientResponse;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import com.google.gson.Gson;

import model.mdlAPIResult;
import model.mdlBranch;
import model.mdlDeviceManagement;
import model.mdlLog;

public class DeviceManagementAdapter {
    final static Logger logger = LogManager.getLogger(DeviceManagementAdapter.class);
    static Gson gson = new Gson();
    static Client client = Client.create();

    public static Integer CheckWSID(String SerialNumber, String WSID) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        model.mdlLog mdlLog = new model.mdlLog();
        mdlLog.ApiFunction = "updateDeviceManagement";
        mdlLog.SystemFunction = functionName;
        mdlLog.LogSource = "Webservice";
        mdlLog.SerialNumber = SerialNumber;
        mdlLog.WSID = WSID;
        mdlLog.LogStatus = "Failed";
        mdlLog.ErrorMessage = "";

        Connection connection = null;
        PreparedStatement pstm = null;
        ResultSet jrs = null;
        Integer returnValue = 0;
        try {
            // define connection
            connection = database.RowSetAdapter.getConnectionWL();
            String sql = "SELECT SerialNumber FROM ms_device WHERE WSID = ?";
            pstm = connection.prepareStatement(sql);
            // insert sql parameter
            pstm.setString(1, WSID);
            // execute query
            jrs = pstm.executeQuery();
            while (jrs.next()) {
                String SerialNumberFromDB = jrs.getString("SerialNumber");
                // if serial number is not same, then return error
                if (!SerialNumber.equalsIgnoreCase(SerialNumberFromDB)) {
                    returnValue = 1;
                }
            }
            mdlLog.LogStatus = "Success";
        } catch (Exception ex) {
            returnValue = 2;
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", ex.toString()), ex);
            mdlLog.ErrorMessage = ex.toString();
            LogAdapter.InsertLog(mdlLog);
        } finally {
            try {
                // close the opened connection
                if (pstm != null)
                    pstm.close();
                if (connection != null)
                    connection.close();
                if (jrs != null)
                    jrs.close();
            } catch (Exception e) {

            }
        }
        return returnValue;
    }

    public static Boolean InsertDeviceManagement(model.mdlDeviceManagement mdlDeviceManagement) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        model.mdlLog mdlLog = new model.mdlLog();
        mdlLog.ApiFunction = "updateDeviceManagement";
        mdlLog.SystemFunction = functionName;
        mdlLog.LogSource = "Webservice";
        mdlLog.SerialNumber = mdlDeviceManagement.SerialNumber;
        mdlLog.WSID = mdlDeviceManagement.WSID;
        mdlLog.LogStatus = "Failed";
        mdlLog.ErrorMessage = "";

        Connection connection = null;
        PreparedStatement pstm = null;
        ResultSet jrs = null;
        boolean isSuccess = false;
        // check if device is exists or not
        try {
            // define connection
            connection = database.RowSetAdapter.getConnectionWL();
            // call store procedure
            String sql = "INSERT INTO ms_device ("
                    + "SerialNumber, WSID, BranchCode, BranchTypeID, BranchInitial, "
                    + "Is_Master,IsMyBCA,UpdateDate,UpdateBy, IP_PRINTER) "
                    + "VALUES (?,?,?,?,?, "
                    + "?,?,CURRENT_TIMESTAMP,?,?)";
            pstm = connection.prepareStatement(sql);

            // insert sql parameter
            pstm.setString(1, mdlDeviceManagement.SerialNumber);
            pstm.setString(2, mdlDeviceManagement.WSID);
            pstm.setString(3, mdlDeviceManagement.BranchCode);
            pstm.setString(4, mdlDeviceManagement.BranchTypeID);
            pstm.setString(5, mdlDeviceManagement.BranchInitial);
            pstm.setString(6, mdlDeviceManagement.IsMaster == null ? "0" : mdlDeviceManagement.IsMaster);
            pstm.setString(7, mdlDeviceManagement.IsMyBca == null ? "0" : mdlDeviceManagement.IsMyBca);
            pstm.setString(8, mdlDeviceManagement.UpdateBy);
            pstm.setString(9, mdlDeviceManagement.IpPrinter);

            // execute query
            jrs = pstm.executeQuery();
            isSuccess = true;
            mdlLog.LogStatus = "Success";
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", ex.toString()), ex);
            mdlLog.ErrorMessage = ex.toString();
            LogAdapter.InsertLog(mdlLog);
        } finally {
            try {
                // close the opened connection
                if (pstm != null)
                    pstm.close();
                if (connection != null)
                    connection.close();
                if (jrs != null)
                    jrs.close();
            } catch (Exception e) {
                logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", e.toString()), e);
            }
        }
        return isSuccess;
    }

    public static Boolean UpdateDeviceManagement(model.mdlDeviceManagement mdlDeviceManagement) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        model.mdlLog mdlLog = new model.mdlLog();
        mdlLog.ApiFunction = "updateDeviceManagement";
        mdlLog.SystemFunction = functionName;
        mdlLog.LogSource = "Webservice";
        mdlLog.SerialNumber = mdlDeviceManagement.SerialNumber;
        mdlLog.WSID = mdlDeviceManagement.WSID;
        mdlLog.LogStatus = "Failed";
        mdlLog.ErrorMessage = "";

        Connection connection = null;
        PreparedStatement pstm = null;
        ResultSet jrs = null;
        boolean isSuccess = false;
        // check if device is exists or not
        try {
            // define connection
            connection = database.RowSetAdapter.getConnectionWL();
            // call store procedure

            String sql = "UPDATE ms_device SET WSID = ?, BranchCode = ?, BranchTypeID = ?, UpdateDate = CURRENT_TIMESTAMP, "
                    + "UpdateBy = ?, BranchInitial = ?, Is_Master = ?, IsMyBCA = ?, IP_PRINTER = ? WHERE SerialNumber = ?";
            pstm = connection.prepareStatement(sql);
            // insert sql parameter
            pstm.setString(1, mdlDeviceManagement.WSID);
            pstm.setString(2, mdlDeviceManagement.BranchCode);
            pstm.setString(3, mdlDeviceManagement.BranchTypeID);
            pstm.setString(4, mdlDeviceManagement.UpdateBy);
            pstm.setString(5, mdlDeviceManagement.BranchInitial);
            pstm.setString(6, mdlDeviceManagement.IsMaster == null ? "0" : mdlDeviceManagement.IsMaster);
            pstm.setString(7, mdlDeviceManagement.IsMyBca == null ? "0" : mdlDeviceManagement.IsMyBca);
            pstm.setString(8, mdlDeviceManagement.IpPrinter == null ? "" : mdlDeviceManagement.IpPrinter);
            pstm.setString(9, mdlDeviceManagement.SerialNumber);

            // execute query
            jrs = pstm.executeQuery();
            isSuccess = true;
            mdlLog.LogStatus = "Success";
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", ex.toString()), ex);
            mdlLog.ErrorMessage = ex.toString();
            LogAdapter.InsertLog(mdlLog);
        } finally {
            try {
                // close the opened connection
                if (pstm != null)
                    pstm.close();
                if (connection != null)
                    connection.close();
                if (jrs != null)
                    jrs.close();
            } catch (Exception e) {
                logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", e.toString()), e);
            }
        }
        return isSuccess;
    }

    public static Boolean DeleteDeviceManagement(model.mdlDeviceManagement mdlDeviceManagement) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        model.mdlLog mdlLog = new model.mdlLog();
        mdlLog.ApiFunction = "updateDeviceManagement";
        mdlLog.SystemFunction = functionName;
        mdlLog.LogSource = "Webservice";
        mdlLog.SerialNumber = mdlDeviceManagement.SerialNumber;
        mdlLog.WSID = mdlDeviceManagement.WSID;
        mdlLog.LogStatus = "Failed";
        mdlLog.ErrorMessage = "";

        Connection connection = null;
        PreparedStatement pstm = null;
        ResultSet jrs = null;
        boolean isSuccess = false;
        // check if device is exists or not
        try {
            // define connection
            connection = database.RowSetAdapter.getConnectionWL();
            // call store procedure

            String sql = "DELETE FROM ms_device WHERE WSID = ? ";
            pstm = connection.prepareStatement(sql);

            // insert sql parameter
            pstm.setString(1, mdlDeviceManagement.WSID);

            // execute query
            jrs = pstm.executeQuery();
            isSuccess = true;
            mdlLog.LogStatus = "Success";
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", ex.toString()), ex);
            mdlLog.ErrorMessage = ex.toString();
            LogAdapter.InsertLog(mdlLog);
        } finally {
            try {
                // close the opened connection
                if (pstm != null)
                    pstm.close();
                if (connection != null)
                    connection.close();
                if (jrs != null)
                    jrs.close();
            } catch (Exception e) {
                logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", e.toString()), e);
            }
        }
        return isSuccess;
    }

    public static boolean CheckDeviceManagement(String SerialNumber, String WSID) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        model.mdlLog mdlLog = new model.mdlLog();
        mdlLog.ApiFunction = "updateDeviceManagement";
        mdlLog.SystemFunction = functionName;
        mdlLog.LogSource = "Webservice";
        mdlLog.SerialNumber = SerialNumber;
        mdlLog.WSID = WSID;
        mdlLog.LogStatus = "Failed";
        mdlLog.ErrorMessage = "";

        Connection connection = null;
        PreparedStatement pstm = null;
        ResultSet jrs = null;
        boolean isExists = false;
        // check if device is exists or not
        try {
            // define connection
            connection = database.RowSetAdapter.getConnectionWL();

            // call store procedure
            String sql = "SELECT SerialNumber FROM ms_device WHERE SerialNumber = ? FETCH FIRST 1 ROWS ONLY";
            pstm = connection.prepareStatement(sql);
            // insert sql parameter
            pstm.setString(1, SerialNumber);
            // execute query
            jrs = pstm.executeQuery();
            while (jrs.next()) {
                isExists = true;
            }
            mdlLog.LogStatus = "Success";
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", ex.toString()), ex);
            mdlLog.ErrorMessage = ex.toString();
            LogAdapter.InsertLog(mdlLog);
        } finally {
            try {
                // close the opened connection
                if (pstm != null)
                    pstm.close();
                if (connection != null)
                    connection.close();
                if (jrs != null)
                    jrs.close();
            } catch (Exception e) {
                logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", e.toString()), e);
            }
        }
        return isExists;
    }

    public static boolean CheckBranchCode(String BranchCode, String BranchTypeID, String BranchInitial, String SerialNumber, String WSID) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        model.mdlLog mdlLog = new model.mdlLog();
        mdlLog.ApiFunction = "updateDeviceManagement";
        mdlLog.SystemFunction = functionName;
        mdlLog.LogSource = "Webservice";
        mdlLog.SerialNumber = SerialNumber;
        mdlLog.WSID = WSID;
        mdlLog.LogStatus = "Failed";
        mdlLog.ErrorMessage = "";

        Connection connection = null;
        PreparedStatement pstm = null;
        ResultSet jrs = null;
        boolean isExists = false;
        try {
            // define connection
            connection = database.RowSetAdapter.getConnectionWL();

            // call store procedure
            String sql = "SELECT BranchCode FROM ms_branch WHERE BranchCode = ? AND BranchTypeID = ? AND BranchInitial = ?";
            pstm = connection.prepareStatement(sql);
            // insert sql parameter
            pstm.setString(1, BranchCode);
            pstm.setString(2, BranchTypeID);
            pstm.setString(3, BranchInitial);
            // execute query
            jrs = pstm.executeQuery();
            while (jrs.next()) {
                isExists = true;
            }
            mdlLog.LogStatus = "Success";
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", ex.toString()), ex);
            mdlLog.ErrorMessage = ex.toString();
            LogAdapter.InsertLog(mdlLog);
        } finally {
            try {
                // close the opened connection
                if (pstm != null)
                    pstm.close();
                if (connection != null)
                    connection.close();
                if (jrs != null)
                    jrs.close();
            } catch (Exception e) {
                logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", e.toString()), e);
            }
        }
        return isExists;
    }

    public static boolean CheckWSIDBranchCode(String BranchCode, String WSID, String SerialNumber) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        model.mdlLog mdlLog = new model.mdlLog();
        mdlLog.ApiFunction = "updateDeviceManagement";
        mdlLog.SystemFunction = functionName;
        mdlLog.LogSource = "Webservice";
        mdlLog.SerialNumber = SerialNumber;
        mdlLog.WSID = WSID;
        mdlLog.LogStatus = "Failed";
        mdlLog.ErrorMessage = "";

        boolean isMatch = false;
        try {

            if (BranchCode.substring(0, 4).contentEquals(WSID.substring(0, 4))) {
                mdlLog.LogStatus = "Success";
                isMatch = true;
            }
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", ex.toString()), ex);
            mdlLog.ErrorMessage = ex.toString();
            LogAdapter.InsertLog(mdlLog);
        }
        return isMatch;
    }

    public static model.mdlDeviceManagement GetDeviceData(String SerialNumber, String WSID) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        model.mdlDeviceManagement deviceData = new model.mdlDeviceManagement();
        model.mdlLog mdlLog = new model.mdlLog();
        mdlLog.ApiFunction = "updateDeviceManagement";
        mdlLog.SystemFunction = functionName;
        mdlLog.LogSource = "Webservice";
        mdlLog.SerialNumber = SerialNumber;
        mdlLog.WSID = WSID;
        mdlLog.LogStatus = "Failed";
        mdlLog.ErrorMessage = "";

        Connection connection = null;
        PreparedStatement pstm = null;
        ResultSet jrs = null;
        try {
            // define connection
            connection = database.RowSetAdapter.getConnectionWL();

            // call store procedure
            String sql = "SELECT WSID,BranchCode,BranchTypeID,BranchInitial,IP_PRINTER FROM ms_device WHERE SerialNumber = ? ";
            pstm = connection.prepareStatement(sql);
            // insert sql parameter
            pstm.setString(1, SerialNumber);
            // execute query
            jrs = pstm.executeQuery();
            while (jrs.next()) {
                deviceData.SerialNumber = SerialNumber;
                deviceData.WSID = jrs.getString("WSID");
                deviceData.BranchCode = jrs.getString("BranchCode");
                deviceData.BranchInitial = jrs.getString("BranchInitial");
                deviceData.BranchTypeID = jrs.getString("BranchTypeID");
                deviceData.IpPrinter = jrs.getString("IP_PRINTER");
            }
            mdlLog.LogStatus = "Success";
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", ex.toString()), ex);
            mdlLog.ErrorMessage = ex.toString();
            LogAdapter.InsertLog(mdlLog);
        } finally {
            try {
                // close the opened connection
                if (pstm != null)
                    pstm.close();
                if (connection != null)
                    connection.close();
                if (jrs != null)
                    jrs.close();
            } catch (Exception e) {
                logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", e.toString()), e);
            }
        }
        return deviceData;
    }

    public static mdlAPIResult hitApiInquiryBranch(String branchCode, String serialNumber, String wsid) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        String apiFunction = "InquiryBranch";
        mdlAPIResult mdlBranchInquiryResult = new mdlAPIResult();
        mdlLog mdlLog = new mdlLog();
        mdlLog.ApiFunction = apiFunction;
        mdlLog.SystemFunction = functionName;
        mdlLog.LogSource = "Middleware";
        mdlLog.SerialNumber = serialNumber;
        mdlLog.WSID = wsid;
        mdlLog.LogStatus = "Success";
        mdlLog.ErrorMessage = "";
        String jsonIn = "";
        String jsonOut = "";
        String urlApi = String.format("/midtier/branches/api/contains?branch-code=%s", branchCode);
        String apiMethod = "GET";
        try {
            // Get the base naming context from web.xml
            Context context = (Context) new InitialContext().lookup("java:comp/env");
            // Get a single value from web.xml
            String urlGetToken = (String) context.lookup("param_url_get_token");
            String ipAddressApiGateway = (String) context.lookup("param_ip_address_api_gateway");
            String keyApi = (String) context.lookup("param_key_api");
            String clientIdApiGateway = EncryptAdapter.decrypt((String) context.lookup("param_client_id_api_gateway"), keyApi);
            String clientSecretApiGateway = EncryptAdapter.decrypt((String) context.lookup("param_client_secret_api_gateway"), keyApi);
            String xBcaClientId = EncryptAdapter.decrypt((String) context.lookup("param_x_bca_client_id"), keyApi);
            String userId = EncryptAdapter.decrypt((String) context.lookup("param_user_id"), keyApi);

            ApiTokenRepository apiTokenRepository = new ApiTokenRepository(urlGetToken, clientIdApiGateway, clientSecretApiGateway, ipAddressApiGateway);
            Map<String, String> headerList = new HashMap<>();
            headerList.put("client-id", xBcaClientId);
            headerList.put("user-id", userId);
            jsonOut = apiTokenRepository.callApiGateway(urlApi, apiMethod, headerList, "");
            if (jsonOut.equals("")) {
                mdlLog.ErrorMessage = "API output is null";
                logger.error(LogAdapter.logToLog4j(false, startTime, 400, urlApi, apiMethod, "function : " + functionName, jsonIn, jsonOut));
            } else {
                mdlBranchInquiryResult = gson.fromJson(jsonOut, mdlAPIResult.class);
                if (mdlBranchInquiryResult.ErrorSchema.ErrorCode == null) {
                    mdlLog.ErrorMessage = "API output is null";
                    logger.error(LogAdapter.logToLog4j(false, startTime, 500, urlApi, apiMethod, "function : " + functionName, jsonIn, jsonOut));
                } else if (!mdlBranchInquiryResult.ErrorSchema.ErrorCode.equalsIgnoreCase("D000")) {
                    mdlLog.ErrorMessage = mdlBranchInquiryResult.ErrorSchema.ErrorMessage.English;
                    logger.error(LogAdapter.logToLog4j(false, startTime, 400, urlApi, apiMethod, "function : " + functionName, jsonIn, jsonOut));
                } else {
                    mdlLog.LogStatus = "Success";
                    logger.info(LogAdapter.logToLog4j(true, startTime, 200, urlApi, apiMethod, "function : " + functionName, jsonIn, jsonOut));
                }
            }
        } catch (Exception ex) {
            mdlBranchInquiryResult = null;
            logger.error(LogAdapter.logToLog4jException(startTime, 500, urlApi, apiMethod, "function : " + functionName, jsonIn, jsonOut, ex.toString()), ex);
            mdlLog.ErrorMessage = ex.toString();
        }
        LogAdapter.InsertLog(mdlLog);
        return mdlBranchInquiryResult;
    }

    public static void DeleteBranchData(mdlDeviceManagement previousDeviceData, String serialNumber, String wsid) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        model.mdlLog mdlLog = new model.mdlLog();
        mdlLog.ApiFunction = "updateDeviceManagement";
        mdlLog.SystemFunction = functionName;
        mdlLog.LogSource = "Webservice";
        mdlLog.SerialNumber = serialNumber;
        mdlLog.WSID = wsid;
        mdlLog.LogStatus = "Failed";
        mdlLog.ErrorMessage = "";

        String sql = "";
        Connection connection = null;
        PreparedStatement pstm = null;
        ResultSet jrs = null;
        try {
            connection = database.RowSetAdapter.getConnectionWL();

            sql = "DELETE FROM ms_branch WHERE branchcode = ? AND branchtypeid = ? AND branchinitial = ? ";

            pstm = connection.prepareStatement(sql);

            pstm.setString(1, previousDeviceData.BranchCode == null ? "" : previousDeviceData.BranchCode);
            pstm.setString(2, previousDeviceData.BranchTypeID == null ? "" : previousDeviceData.BranchTypeID);
            pstm.setString(3, previousDeviceData.BranchInitial == null ? "" : previousDeviceData.BranchInitial);
            jrs = pstm.executeQuery();
            mdlLog.LogStatus = "Success";
            logger.info(LogAdapter.logToLog4j(true, startTime, 200, "BCAUpdateDevice", "POST", "Function : " + functionName + ", serial : " + serialNumber
                    + ", wsid : " + wsid + ", previousDeviceData : " + gson.toJson(previousDeviceData), "", ""));
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "Function : " + functionName + ", serial : " + serialNumber
                    + ", wsid : " + wsid + ", previousDeviceData : " + gson.toJson(previousDeviceData), "", "", ex.toString()), ex);
            mdlLog.ErrorMessage = ex.toString();
            LogAdapter.InsertLog(mdlLog);
        } finally {
            try {
                // close the opened connection
                if (pstm != null)
                    pstm.close();
                if (connection != null)
                    connection.close();
                if (jrs != null)
                    jrs.close();
            } catch (Exception e) {
                logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", e.toString()), e);
            }
        }
    }

    public static boolean UpdateBranchData(mdlDeviceManagement previousDeviceData, mdlBranch branch, String serialNumber, String wsid) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        model.mdlLog mdlLog = new model.mdlLog();
        mdlLog.ApiFunction = "updateDeviceManagement";
        mdlLog.SystemFunction = functionName;
        mdlLog.LogSource = "Webservice";
        mdlLog.SerialNumber = serialNumber;
        mdlLog.WSID = wsid;
        mdlLog.LogStatus = "Failed";
        mdlLog.ErrorMessage = "";
        boolean success = false;

        String sql = "";
        Connection connection = null;
        PreparedStatement pstm = null;
        ResultSet jrs = null;
        try {
            connection = database.RowSetAdapter.getConnectionWL();

            sql = "call SP_UPSERT_BRANCH_DATA(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?)";
            pstm = connection.prepareStatement(sql);

            pstm.setString(1, previousDeviceData.BranchCode == null ? "" : previousDeviceData.BranchCode);
            pstm.setString(2, previousDeviceData.BranchTypeID == null ? "" : previousDeviceData.BranchTypeID);
            pstm.setString(3, previousDeviceData.BranchInitial == null ? "" : previousDeviceData.BranchInitial);
            pstm.setString(4, branch.BranchCode);
            pstm.setString(5, branch.BranchName);

            pstm.setString(6, branch.RegionCode);
            pstm.setString(7, branch.BranchTypeID);
            pstm.setString(8, branch.BranchCoordinator);
            pstm.setString(9, branch.BranchInitial);
            pstm.setString(10, wsid);

            pstm.setString(11, branch.LocationType);
            pstm.setString(12, branch.Address);
            pstm.setString(13, branch.City);
            pstm.setString(14, branch.Longitude);
            pstm.setString(15, branch.Latitude);

            pstm.setString(16, branch.FlagReservation);
            pstm.setString(17, branch.FlagWeekendBankingSaturday);
            pstm.setString(18, branch.FlagWeekendBankingSunday);
            pstm.setString(19, branch.VendorKiosk);
            pstm.setString(20, branch.RegularKiosk);
            pstm.setString(21, branch.PrioritasKiosk);
            pstm.setString(22, branch.Timezone);
            pstm.setString(23, branch.KcuCode);

            // execute query
            jrs = pstm.executeQuery();
            success = true;
            mdlLog.LogStatus = "Success";
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "Function : " + functionName + ", serial : " + serialNumber
                    + ", wsid : " + wsid + ", mdlBranch : " + gson.toJson(branch), "", "", ex.toString()), ex);
            mdlLog.ErrorMessage = ex.toString();
            LogAdapter.InsertLog(mdlLog);
        } finally {
            try {
                // close the opened connection
                if (pstm != null)
                    pstm.close();
                if (connection != null)
                    connection.close();
                if (jrs != null)
                    jrs.close();
            } catch (Exception e) {
                logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", e.toString()), e);
            }
        }
        return success;
    }

    public static void InsertDeviceManagementHistory(mdlDeviceManagement previousDevice, mdlDeviceManagement currentDevice) {
        long startTime = System.currentTimeMillis();
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
        model.mdlLog mdlLog = new model.mdlLog();
        mdlLog.ApiFunction = "updateDeviceManagement";
        mdlLog.SystemFunction = functionName;
        mdlLog.LogSource = "Webservice";
        mdlLog.SerialNumber = currentDevice.SerialNumber;
        mdlLog.WSID = currentDevice.WSID;
        mdlLog.LogStatus = "Failed";
        mdlLog.ErrorMessage = "";

        String sql = "";
        Connection connection = null;
        PreparedStatement pstm = null;
        ResultSet jrs = null;
        try {
            connection = database.RowSetAdapter.getConnectionWL();
            sql = "INSERT INTO ms_device_history (SERIALNUMBER, PREV_WSID, PREV_BRANCHCODE, PREV_BRANCHTYPEID, PREV_BRANCHINITIAL, "
                    + "NEW_WSID, NEW_BRANCHCODE, NEW_BRANCHTYPEID, NEW_BRANCHINITIAL, PREV_IP_PRINTER, "
                    + "NEW_IP_PRINTER ) "
                    + "VALUES (?,?,?,?,?, ?,?,?,?,?, ?)";
            pstm = connection.prepareStatement(sql);
            pstm.setString(1, previousDevice.SerialNumber == null ? "" : previousDevice.SerialNumber);
            pstm.setString(2, previousDevice.WSID == null ? "" : previousDevice.WSID);
            pstm.setString(3, previousDevice.BranchCode == null ? "" : previousDevice.BranchCode);
            pstm.setString(4, previousDevice.BranchTypeID == null ? "" : previousDevice.BranchTypeID);
            pstm.setString(5, previousDevice.BranchInitial == null ? "" : previousDevice.BranchInitial);

            pstm.setString(6, currentDevice.WSID);
            pstm.setString(7, currentDevice.BranchCode);
            pstm.setString(8, currentDevice.BranchTypeID);
            pstm.setString(9, currentDevice.BranchInitial);
            pstm.setString(10, previousDevice.IpPrinter);

            pstm.setString(11, currentDevice.IpPrinter);

            jrs = pstm.executeQuery();
            mdlLog.LogStatus = "Success";
            logger.info(LogAdapter.logToLog4j(true, startTime, 200, "BCAUpdateDevice", "POST", "function: " + functionName + ", previousDevice : "
                    + gson.toJson(previousDevice) + ", currentDevice : " + gson.toJson(currentDevice), "", ""));
        } catch (Exception ex) {
            logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName + ", previousDevice : "
                    + gson.toJson(previousDevice) + ", currentDevice : " + gson.toJson(currentDevice), "", "", ex.toString()), ex);
            mdlLog.ErrorMessage = ex.toString();
            LogAdapter.InsertLog(mdlLog);
        } finally {
            try {
                // close the opened connection
                if (pstm != null)
                    pstm.close();
                if (connection != null)
                    connection.close();
                if (jrs != null)
                    jrs.close();
            } catch (Exception e) {
                logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", e.toString()), e);
            }
        }
    }
}