package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class LogAdapter {
    final static Logger logger = LogManager.getLogger(LogAdapter.class);

    public static void InsertLog(model.mdlLog logModel) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	try {
	    // logic logID baru
	    String dateNowString = ConvertDateTimeHelper.GetDateTimeNowCustomFormat("yyyyMMddHHmmssSSS");
	    StringBuilder sb = new StringBuilder();
	    sb.append("API-").append(dateNowString).append("-").append(logModel.WSID);
	    String newLogID = sb.toString();

	    // define connection
	    connection = database.RowSetAdapter.getConnectionWL();
	    String sql = "INSERT INTO APILog (LogID, LogDate, LogSource, SerialNumber, WSID, APIFunction, SystemFunction, LogStatus, ErrorMessage)"
		    + "VALUES (?,TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF'),?,?,?,?,?,?,?)";

	    pstm = connection.prepareStatement(sql);

	    // insert sql parameter
	    String dateNow = LocalDateTime.now().toString().replace("T", " ");
	    pstm.setString(1, newLogID);
	    pstm.setString(2, dateNow);
	    pstm.setString(3, logModel.LogSource);
	    pstm.setString(4, logModel.SerialNumber);
	    pstm.setString(5, logModel.WSID);
	    pstm.setString(6, logModel.ApiFunction);
	    pstm.setString(7, logModel.SystemFunction);
	    pstm.setString(8, logModel.LogStatus);
	    pstm.setString(9, logModel.ErrorMessage);

	    // execute query
	    jrs = pstm.executeQuery();
	} catch (Exception ex) {
	    logger.error(logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", ex.toString()), ex);
	} finally {
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception e) {
		logger.error(logToLog4jException(startTime, 500, "BCAUpdateDevice", "POST", "function: " + functionName, "", "", e.toString()), e);
	    }
	}
	return;
    }

    public static String logToLog4jException(long startTime, long responseStatus, String urlAPI, String method, String customData, String jsonIn,
	    String jsonOut, String exception) {
	long elapsedTime = System.currentTimeMillis() - startTime;
	return "FAILED = responseTime: " + elapsedTime + "ms, responseStatus: 500, URL: " + urlAPI + ", method: GET, " + customData
		+ " ~ jsonIn: " + jsonIn + ", jsonOut: " + jsonOut + ", exception: " + exception;

    }

    public static String logToLog4j(boolean success, long startTime, long responseStatus, String urlAPI, String method, String customData, String jsonIn,
	    String jsonOut) {
	long elapsedTime = System.currentTimeMillis() - startTime;
	if (success) {
	    return "SUCCESS = responseTime: " + elapsedTime + "ms, responseStatus: " + responseStatus + ", URL: " + urlAPI + ", method: " + method
		    + ", " + customData + " ~ jsonIn: " + jsonIn + ", jsonOut: " + jsonOut;
	} else {
	    return "FAILED = responseTime: " + elapsedTime + "ms, responseStatus: " + responseStatus + ", URL: " + urlAPI + ", method: " + method
		    + ", " + customData + " ~ jsonIn: " + jsonIn + ", jsonOut: " + jsonOut + "";
	}
    }

    public static String logControllerToLog4j(boolean success, long startTime, long responseStatus, String url, String method, String customData,
	    String jsonIn, String jsonOut) {
	long elapsedTime = System.currentTimeMillis() - startTime;

	if (success) {
	    return "SUCCESS = responseTime: " + elapsedTime + "ms, responseStatus: " + responseStatus + ", URL: " + url + ", method: " + method
		    + ", " + customData + " ~ jsonIn: " + jsonIn + ", jsonOut: " + jsonOut;
	} else {
	    return "FAILED = responseTime: " + elapsedTime + "ms, responseStatus: " + responseStatus + ", URL: " + url + ", method: " + method
		    + ", " + customData + " ~ jsonIn: " + jsonIn + ", jsonOut: " + jsonOut;
	}
    }

    public static String logControllerToLog4jException(long startTime, long responseStatus, String url, String method, String customData,
	    String jsonIn, String jsonOut, String exception) {
	long elapsedTime = System.currentTimeMillis() - startTime;
	return "FAILED = responseTime: " + elapsedTime + "ms, responseStatus: " + responseStatus + ", URL: " + url + ", method: " + method
		+ ", " + customData + " ~ jsonIn: " + jsonIn + ", jsonOut: " + jsonOut + ", exception: " + exception;
    }
}
