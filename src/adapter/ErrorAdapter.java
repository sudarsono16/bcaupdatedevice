package adapter;

import model.mdlErrorSchema;
import model.mdlMessage;

public class ErrorAdapter {
    public static mdlErrorSchema getErrorSchemaPing(String errorCode) {
        mdlErrorSchema mdlErrorSchema = new mdlErrorSchema();
        mdlErrorSchema.ErrorCode = errorCode;
        mdlMessage mdlMessage = new mdlMessage();
        switch (errorCode) {
            case "00":
                mdlMessage.Indonesian = "Sukses";
                mdlMessage.English = "Success";
                break;
            default:
                mdlErrorSchema.ErrorCode = "99";
                mdlMessage.Indonesian = "Gagal memanggil service";
                mdlMessage.English = "Service call failed";
                break;
        }
        mdlErrorSchema.ErrorMessage = mdlMessage;
        return mdlErrorSchema;
    }

    public static model.mdlErrorSchema GetErrorSchema(String errorCode) {
        mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
        mdlErrorSchema.ErrorCode = errorCode;
        mdlMessage mdlMessage = new model.mdlMessage();
        switch (errorCode) {
            case "00":
                mdlMessage.Indonesian = "Sukses";
                mdlMessage.English = "Success";
                break;
            case "01":
                mdlMessage.Indonesian = "Tidak ada data cabang dari API Gateway.";
                mdlMessage.English = "There is no branch data from API Gateway.";
                break;
            case "02":
                mdlMessage.Indonesian = "Pengecekan WSID gagal";
                mdlMessage.English = "WSID check failed";
                break;
            case "04":
                mdlMessage.Indonesian = "Kode Cabang Dan WSID Tidak Sama";
                mdlMessage.English = "Branch Code And WSID Not Matched";
                break;
            case "05":
                mdlMessage.Indonesian = "WSID Sudah Ada";
                mdlMessage.English = "WSID Already Exists";
                mdlErrorSchema.ErrorMessage = mdlMessage;
                break;
            case "06":
                mdlMessage.Indonesian = "Gagal memanggil service HitAPIBranchInquiryEAI";
                mdlMessage.English = "Service HitAPIBranchInquiryEAI call failed";
                break;
            case "07":
                mdlMessage.Indonesian = "Gagal update data cabang EAI ke database.";
                mdlMessage.English = "Update EAI branch data to database failed.";
                break;
            case "08":
                mdlMessage.Indonesian = "Data cabang tidak ditemukan di data cabang EAI.";
                mdlMessage.English = "Branch data not found in EAI branch data.";
                break;
            default:
                mdlMessage.Indonesian = "Gagal memanggil service";
                mdlMessage.English = "Service call failed";
                break;
        }
        mdlErrorSchema.ErrorMessage = mdlMessage;
        return mdlErrorSchema;
    }
}
